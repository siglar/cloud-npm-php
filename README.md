cloud-npm-php


In google cloud VM Instance creation:

**Automation:**

```
#! /bin/bash
cd ~
sudo apt-get install -y nodejs-legacy
sudo apt-get install -y npm
sudo apt-get install -y php-cgi
git clone https://gitlab.com/siglar/cloud-npm-php.git
mkdir -p public
cd ./cloud-npm-php
sudo npm install -g forever
npm install
sudo forever start ./bin/www
cd ../public
```

Now Server should run in folder ~./public

**Use:**

```
google_metadata_script_runner --script-type startup
```

In console to rerun script